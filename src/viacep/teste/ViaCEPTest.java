/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viacep.teste;

import java.util.Scanner;
import br.com.parg.viacep.*;

/**
 *
 * @author PABLO
 */
public class ViaCEPTest implements ViaCEPEvents {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new ViaCEPTest().run();
    }

    public void run() {
        ViaCEP viaCEP = new ViaCEP(this);
        Scanner scan = new Scanner(System.in);
        String cmd;

        System.out.println(viaCEP.getClass().getName() + " v" + String.valueOf(ViaCEP.VIACEP_VERSAO) + " - digite 0 para fechar o teste!");

        do {
            System.out.println();
            System.out.println("O que você deseja fazer:");
            System.out.println("  1 - buscar endereço usando CEP");
            System.out.println("  2 - buscar CEP usando endereço");
            System.out.println("  3 - mostrar todos os CEP's consultados");
            System.out.println("  4 - teste usando interface gráfica Swing");
            System.out.println("  5 - teste de benchmark");
            System.out.println("  0 - sair");
            System.out.print("> ");

            cmd = scan.next();

            System.out.println();

            switch (cmd) {
                case "1":
                    cep(viaCEP, scan);
                    break;
                case "2":
                    endereco(viaCEP, scan);
                    break;
                case "3":
                    lista(viaCEP);
                    break;
                case "4":
                    new Teste().run();
                    break;
                case "5":
                    new TesteBenchmark().run();
                    break;
                default:
                    System.out.println("é necessário escolher uma das opções!");
            }
        } while (!cmd.equals("0"));
    }

    public void cep(ViaCEP viaCEP, Scanner scan) {
        String cep;

        System.out.print("Digite um cep: ");
        cep = scan.next();
        // consulta
        try {
            viaCEP.buscar(cep);
        } catch (ViaCEPException ex) {
            System.out.println("Ocorreu um erro na classe " + ex.getClasse() + ": " + ex.getMessage());
        }
    }

    public void endereco(ViaCEP viaCEP, Scanner scan) {
        String uf, localidade, logradouro;

        // obtem os dados
        System.out.print("Digite o UF: ");
        uf = scan.next();
        System.out.print("Digite a Cidade: ");
        localidade = scan.next();
        System.out.print("Digite o Logradouro: ");
        logradouro = scan.next();
        System.out.println();

        // consulta
        try {
            viaCEP.buscarCEP(uf, localidade, logradouro);

            System.out.println("Tem um total de " + String.valueOf(viaCEP.getSize()) + " CEP's consultados!");
        } catch (ViaCEPException ex) {
            System.out.println("Ocorreu um erro na classe " + ex.getClasse() + ": " + ex.getMessage());
        }
    }

    public void lista(ViaCEP viaCEP) {
        if (viaCEP.moveFirst()) {
            do {
                onCEPSuccess(viaCEP);
            } while (viaCEP.moveNext());
        } else {
            System.out.println("Nenhum CEP encontrado!");
        }
    }

    @Override
    public void onCEPSuccess(ViaCEP cep) {
        System.out.println();
        System.out.println("CEP " + cep.getCep() + " encontrado!");
        System.out.println("Logradouro: " + cep.getLogradouro());
        System.out.println("Complemento: " + cep.getComplemento());
        System.out.println("Bairro: " + cep.getBairro());
        System.out.println("Localidade: " + cep.getLocalidade());
        System.out.println("UF: " + cep.getUf());
        System.out.println("Gia: " + cep.getGia());
        System.out.println("Ibge: " + cep.getIbge());
        System.out.println();
    }

    @Override
    public void onCEPError(String cep) {
        System.out.println();
        System.out.println("Não foi possível encontrar o CEP " + cep + "!");
        System.out.println();
    }

}
